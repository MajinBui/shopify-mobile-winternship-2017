package com.vanchaubui.shopifydevelopmentproblem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView amountSpent;
    private TextView bagsSold;
    private String url;
    private Button calculateButton;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // TextViews that will show the result calculations
        this.amountSpent = (TextView) findViewById(R.id.amount_spent);
        this.bagsSold = (TextView) findViewById(R.id.bags_sold);
        this.url = "https://shopicruit.myshopify.com/admin/orders.json?page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6";
        this.calculateButton = (Button) findViewById(R.id.calculate_button);
        this.calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCalculateClick(view);
            }
        });
        // Should be a singleton but the app doesn't require one..... *yet*
        this.requestQueue = Volley.newRequestQueue(this.getApplicationContext());
    }

    /**
     * OnClick function for the calculate button that will grab data from the Shopify API endpoint
     * and find out how much the user ID 4953626051 (Napoleon Batz) spent and how many Item ID
     * 2759139395 (Awesome Bronze Bags) was bought
     * @param v The View that triggered the function
     */
    public void onCalculateClick(View v) {
        // Create the request object to access data from the endpoint
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, this.url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // Try to parse the data
                        try{
                            JSONArray orders = response.getJSONArray("orders");
                            double totalNapSpent = 0;
                            int totalAwesomeBronzeBagsSold = 0;
                            for (int i = 0; i < orders.length(); i++) {
                                JSONObject order = (JSONObject) orders.get(i);
                                // Check customer info if the order was by Napoleon
                                try {
                                    JSONObject customer = (JSONObject) order.get("customer");
                                    String customerID = Long.toString(customer.getLong("id"));
                                    // Add Nap's total if user is Napoleon
                                    if (customerID.equals("4953626051")) {
                                        totalNapSpent += order.getDouble("total_price");
                                    }
                                } catch (JSONException e) {
                                    Log.d("", e.getMessage());
                                }
                                // Check items sold for Awesome Bronze Bags
                                try {
                                    JSONArray lineItems = (JSONArray) order.get("line_items");
                                    for (int j = 0; j < lineItems.length(); j++) {
                                        JSONObject lineItem = (JSONObject) lineItems.get(j);
                                        String itemID = Long.toString(lineItem.getLong("product_id"));
                                        // Add to total sold if ID matches
                                        if (itemID.equals("2759139395")) {
                                            totalAwesomeBronzeBagsSold += lineItem.getInt("quantity");
                                        }
                                    }
                                } catch (JSONException e) {
                                    Log.d("", e.getMessage());
                                }
                            }
                            // Set the text
                            amountSpent.setText("$" + Double.toString(totalNapSpent));
                            bagsSold.setText(Integer.toString(totalAwesomeBronzeBagsSold));
                            Toast.makeText(getBaseContext(), "Whoa, Napoleon is a big spender!", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            Log.d("", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("", error.getMessage());
                    }
                });

        this.requestQueue.add(jsObjRequest);
    }
}
