# Shopify Mobile Winternship 2018 Challenge
This app was made to complete the Shopify Winternship challenge for 2018

## The task
Using this [endpoint](!https://shopicruit.myshopify.com/admin/orders.json?page=1&access_token=c32313df0d0ef512ca64d5b336a0d7c6), determine how much the customer Napoleon Batz spends and how many Awesome Bronze Bags were sold.

## Results
<img src="./Output.png" width=300>